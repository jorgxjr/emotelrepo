package source.open.TrabajoFinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import source.open.TrabajoFinal.entities.ReservaRestaurante;

@Repository
public interface IReservaRestauranteRepository extends JpaRepository<ReservaRestaurante, Integer>{

}
