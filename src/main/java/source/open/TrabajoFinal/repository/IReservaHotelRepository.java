package source.open.TrabajoFinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import source.open.TrabajoFinal.entities.ReservaHotel;
@Repository
public interface IReservaHotelRepository extends JpaRepository<ReservaHotel, Integer>{

}
