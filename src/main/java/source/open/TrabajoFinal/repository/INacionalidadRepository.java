package source.open.TrabajoFinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import source.open.TrabajoFinal.entities.Nacionalidad;

@Repository
public interface INacionalidadRepository extends JpaRepository<Nacionalidad, Integer>{
	
}
