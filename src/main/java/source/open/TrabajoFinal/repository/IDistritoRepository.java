package source.open.TrabajoFinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import source.open.TrabajoFinal.entities.Distrito;


@Repository
public interface IDistritoRepository extends JpaRepository<Distrito, Integer>{
	
}
