package source.open.TrabajoFinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import source.open.TrabajoFinal.entities.Provincia;

@Repository
public interface IProvinciaRepository extends JpaRepository<Provincia, Integer>{

}
