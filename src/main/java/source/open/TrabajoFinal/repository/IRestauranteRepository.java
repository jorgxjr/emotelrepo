package source.open.TrabajoFinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import source.open.TrabajoFinal.entities.Restaurante;

@Repository
public interface IRestauranteRepository extends JpaRepository<Restaurante, Integer>{

}
