package source.open.TrabajoFinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import source.open.TrabajoFinal.entities.Hotel;

@Repository
public interface IHotelRepository extends JpaRepository<Hotel, Integer>{

}
