package source.open.TrabajoFinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import source.open.TrabajoFinal.entities.TipoHotel;

@Repository
public interface ITipoHotelRepository extends JpaRepository<TipoHotel, Integer>{

}
