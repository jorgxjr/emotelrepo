package source.open.TrabajoFinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import source.open.TrabajoFinal.entities.Administrador;

@Repository
public interface IAdministradorRepository extends JpaRepository<Administrador, Integer>{

	public Administrador buscarPorCorreo(String correo,String contraseña);
	
	public boolean existeCorreo(String correo);
}
