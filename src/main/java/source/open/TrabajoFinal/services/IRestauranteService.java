package source.open.TrabajoFinal.services;

import java.util.List;

import org.springframework.stereotype.Service;

import source.open.TrabajoFinal.entities.Restaurante;

@Service
public interface IRestauranteService {

	public boolean agregar(Restaurante restaurante);
	public List<Restaurante> listar();
	public boolean eliminar(int id);
	public Restaurante cargarRestaurante(int id);
}
