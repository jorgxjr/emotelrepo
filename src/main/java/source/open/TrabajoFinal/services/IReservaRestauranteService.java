package source.open.TrabajoFinal.services;

import java.util.List;

import org.springframework.stereotype.Service;

import source.open.TrabajoFinal.entities.ReservaRestaurante;

@Service
public interface IReservaRestauranteService {
	public boolean agregar(ReservaRestaurante reservarest);
	public List<ReservaRestaurante> listar();
	public boolean eliminar(int id);
	public ReservaRestaurante cargarReservaRestaurante(int id);
}
