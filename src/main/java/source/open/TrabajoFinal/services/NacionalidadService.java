package source.open.TrabajoFinal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import source.open.TrabajoFinal.entities.Nacionalidad;
import source.open.TrabajoFinal.repository.INacionalidadRepository;

@Service
public class NacionalidadService implements INacionalidadService {
	@Autowired
	INacionalidadRepository repository;
	@Override
	public List<Nacionalidad> listar() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

}
