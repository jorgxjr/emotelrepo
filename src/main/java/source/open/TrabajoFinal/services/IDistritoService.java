package source.open.TrabajoFinal.services;

import java.util.List;

import org.springframework.stereotype.Service;

import source.open.TrabajoFinal.entities.Distrito;

@Service
public interface IDistritoService {

	public boolean agregar(Distrito distrito);
	public List<Distrito> listar();
}
