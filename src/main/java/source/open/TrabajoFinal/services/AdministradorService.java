package source.open.TrabajoFinal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import source.open.TrabajoFinal.entities.Administrador;
import source.open.TrabajoFinal.repository.IAdministradorRepository;

@Service
public class AdministradorService implements IAdministradorService {
	@Autowired
	IAdministradorRepository repository;
	
	@Override
	public boolean agregar(Administrador usuario) {
		boolean flag = false;
		
		try {
			Administrador objUsuario= repository.save(usuario);
			
			if(objUsuario != null) {
				flag = true;
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return flag;
	}

	@Override
	public List<Administrador> listar() {
		return repository.findAll();
	}
	@Override
	public Administrador buscarPorCorreo(String correo,String contraseña) {
		
		return repository.buscarPorCorreo(correo,contraseña);
	}

	@Override
	public boolean existeCorreo(String correo) {
		// TODO Auto-generated method stub
		return repository.existeCorreo(correo);
	}
}
