package source.open.TrabajoFinal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import source.open.TrabajoFinal.entities.ReservaHotel;

import source.open.TrabajoFinal.repository.IReservaHotelRepository;


@Service
public class ReservaHotelService implements IReservaHotelService {

	@Autowired
	IReservaHotelRepository repository;
	@Override
	public boolean agregar(ReservaHotel reservahotel) {
		boolean flag = false;
		try {
			ReservaHotel objReservaHotel= repository.save(reservahotel);
			if(objReservaHotel!= null) {
				flag = true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return flag;
	}

	@Override
	public List<ReservaHotel> listar() {
		return repository.findAll();
	}

	@Override
	public boolean eliminar(int id) {
		boolean flag = false;
		try {
			repository.deleteById(id);
			flag = true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return flag;
	}

	@Override
	public ReservaHotel cargarReservaHotel(int id) {
		ReservaHotel objReserva = null;
		try {
			objReserva = repository.getOne(id);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return objReserva;
	}

}
