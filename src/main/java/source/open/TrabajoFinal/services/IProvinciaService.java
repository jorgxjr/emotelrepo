package source.open.TrabajoFinal.services;

import java.util.List;

import org.springframework.stereotype.Service;

import source.open.TrabajoFinal.entities.Provincia;

@Service
public interface IProvinciaService {

	public boolean agregar(Provincia provincia);
	public List<Provincia> listar();
}
