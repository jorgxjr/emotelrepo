package source.open.TrabajoFinal.services;

import java.util.List;

import org.springframework.stereotype.Service;

import source.open.TrabajoFinal.entities.Administrador;

@Service
public interface IAdministradorService {

	public boolean agregar(Administrador usuario);
	public List<Administrador> listar();
	public Administrador buscarPorCorreo(String correo,String contraseña);
	public boolean existeCorreo(String correo);
}
