package source.open.TrabajoFinal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import source.open.TrabajoFinal.entities.Usuario;

import source.open.TrabajoFinal.repository.IUsuarioRepository;

@Service
public class UsuarioService implements IUsuarioService {

	@Autowired
	IUsuarioRepository repository;
	@Override
	public boolean agregar(Usuario usuario) {
		boolean flag = false;
		try {
			Usuario objUsuario= repository.save(usuario);
			if(objUsuario != null) {
				flag = true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return flag;
	}

	@Override
	public List<Usuario> listar() {
		return repository.findAll();
	}

	@Override
	public Usuario buscarPorCorreo(String correo, String password) {
		return repository.buscarPorCorreo(correo, password);
	}

	@Override
	public Usuario existeCorreo(String correo) {
		return repository.existeCorreo(correo);
	}

	@Override
	public Usuario cargarUsuario(int id) {
		Usuario objUsuario = null;
		try {
			objUsuario = repository.getOne(id);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return objUsuario;
	}

}
