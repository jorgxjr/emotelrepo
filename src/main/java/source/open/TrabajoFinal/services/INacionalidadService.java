package source.open.TrabajoFinal.services;

import java.util.List;

import org.springframework.stereotype.Service;

import source.open.TrabajoFinal.entities.Nacionalidad;


@Service
public interface INacionalidadService {
	public List<Nacionalidad> listar();
}
