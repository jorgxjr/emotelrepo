package source.open.TrabajoFinal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import source.open.TrabajoFinal.entities.ReservaRestaurante;

import source.open.TrabajoFinal.repository.IReservaRestauranteRepository;

@Service
public class ReservaRestauranteService implements IReservaRestauranteService {
	@Autowired
	IReservaRestauranteRepository repository;
	@Override
	public boolean agregar(ReservaRestaurante reservarest) {
		boolean flag = false;
		try {
			ReservaRestaurante objReservaRestaurante= repository.save(reservarest);
			if(objReservaRestaurante!= null) {
				flag = true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return flag;
	}

	@Override
	public List<ReservaRestaurante> listar() {
		return repository.findAll();
	}

	@Override
	public boolean eliminar(int id) {
		boolean flag = false;
		try {
			repository.deleteById(id);
			flag = true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return flag;
	}

	@Override
	public ReservaRestaurante cargarReservaRestaurante(int id) {
		ReservaRestaurante objReserva = null;
		try {
			objReserva = repository.getOne(id);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return objReserva;
	}

}
